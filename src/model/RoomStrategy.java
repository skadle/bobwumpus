package model;

public interface RoomStrategy {
	public int enterRoom();
}
