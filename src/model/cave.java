package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * -1 safe bats
 * -2 wumpus death
 * -3 pit death
 */

public class cave{
	private boolean[][] map;
	private List<RoomStrategy> rooms;
	private int location;
	private int vertex;
	
	public cave(){
		Random random = new Random();
		vertex = 20;
		setupCave();
		rooms = new ArrayList();
		
		for(int a=0; a<vertex; a++){
			rooms.add(new EmptyRoom());
		}
		
		List<RoomStrategy> list = new ArrayList();
		list.add(new WumpusRoom());
		list.add(new BatRoom());
		list.add(new BatRoom());
		list.add(new PitRoom());
		list.add(new PitRoom());
		
		for(int a=0; a<list.size(); a++){
			int temp = random.nextInt()%vertex;
			
			if(rooms.get(temp).equals(new EmptyRoom())){
				rooms.set(temp, list.get(a));
			}
			else{
				a--;
			}
		}
		
		location = random.nextInt()%vertex;
	}
	
	public cave(boolean temp){
		vertex = 20;
		setupCave();
		rooms = new ArrayList();
		
		for(int a=0; a<vertex; a++){
			rooms.add(new EmptyRoom());
		}
		
		rooms.set(0, new BatRoom());
		rooms.set(10, new BatRoom());
		rooms.set(6, new PitRoom());
		rooms.set(18, new PitRoom());
		rooms.set(15, new WumpusRoom());
		
		location = 1;
	}
	
	private void setupCave(){
		map = new boolean[20][20];

		map[0][1] = map[0][19] = map[0][4] = true;
		map[1][0] = map[1][2] = map[1][9] = true;
		map[2][1] = map[2][3] = map[2][11] = true;
		map[3][4] = map[3][2] = map[3][13] = true;
		map[4][0] = map[4][5] = map[4][3] = true;
		map[5][4] = map[5][6] = map[5][14] = true;
		map[6][19] = map[6][5] = map[6][7] = true;
		map[7][6] = map[7][15] = map[7][18] = true;
		map[8][9] = map[8][19] = map[8][18] = true;
		map[9][8] = map[9][1] = map[9][10] = true;
		map[10][9] = map[10][11] = map[10][17] = true;
		map[11][10] = map[11][2] = map[11][12] = true;
		map[19][0]=map[19][6]=map[19][8]=true;
		map[18][17]=map[18][7]=map[18][8]=true;
		map[17][18]=map[17][16]=map[17][10]=true;
		map[16][17]=map[16][12]=map[16][15]=true;
		map[15][14]=map[15][7]=map[15][16]=true;
		map[14][15]=map[14][13]=map[14][5]=true;
		map[13][3]=map[13][12]=map[13][14]=true;
		map[12][13]=map[12][16]=map[12][11]=true;
	}
	
	/**
	 * isBatNear() -- 
	 * 
	 * this method checks the room directly adjacent to the person to see if
	 * bats are in one of the adjacent rooms.
	 * 
	 * @return boolean true if there are bats in an adjacent room
	 */
	public boolean isBatNear(){
		List<Integer> list = new ArrayList();
//		BatRoom tempRoom = new BatRoom();
		
		for(int a=0; a<map.length; a++){
			if(map[location][a]){
				list.add(a);
			}
		}
		
		for(int a=0; a<list.size(); a++){
			if(rooms.get(list.get(a)) instanceof BatRoom){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * isWumpusNear() -- 
	 * 
	 * this method checks the room directly adjacent to the person to see if
	 * a wumpus is in one of the adjacent rooms.
	 * 
	 * @return boolean true if there is a wumpus in an adjacent room
	 */
	public boolean isWumpusNear(){
		List<Integer> list = new ArrayList();
//		WumpusRoom tempRoom = new WumpusRoom();
		
		for(int a=0; a<map.length; a++){
			if(map[location][a]){
				list.add(a);
			}
		}
		
		for(int a=0; a<list.size(); a++){
			if(rooms.get(list.get(a)) instanceof WumpusRoom){
				return true;
			}
		}
		return false;
	}

	/**
	 * isPitNear() -- 
	 * 
	 * this method checks the room directly adjacent to the person to see if
	 * a pit is in one of the adjacent rooms.
	 * 
	 * @return boolean true if there is a pit in an adjacent room
	 */
	public boolean isPitNear(){
		List<Integer> list = new ArrayList();
//		PitRoom tempRoom = new PitRoom();
		
		for(int a=0; a<map.length; a++){
			if(map[location][a]){
				list.add(a);
			}
		}
		
		for(int a=0; a<list.size(); a++){
			if(rooms.get(list.get(a)) instanceof PitRoom){
				return true;
			}
		}
		return false;
	}
	
	public void move(int newLocation){
		location = newLocation;
		
		int result = rooms.get(location).enterRoom();
	}
	
	/**
	 * getCurrentRoom() -- 
	 * 
	 * this method returns the room the person is currently located in.
	 * it returns the room number in the form of an int
	 * 
	 * @return the room the person is currently in, in the form of an int
	 */
	public int getCurrentRoom(){
		return location;
	}
	
	public String RoomToString(int room){
		if(rooms.get(room) instanceof EmptyRoom){
			return "empty room";
		}
		else if(rooms.get(room) instanceof PitRoom){
			return "pit room";
		}
		else if(rooms.get(room) instanceof BatRoom){
			return "bat room";
		}
		else if(rooms.get(room) instanceof WumpusRoom){
			return "wumpus room";
		}
		else{
			return "steve did something wrong -.-";
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}