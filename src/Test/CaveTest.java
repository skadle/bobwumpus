package Test;
import static org.junit.Assert.*;
import model.*;

import org.junit.Test;

public class CaveTest {
	
	@Test
	public void caveSetUpTest(){
		cave cave = new cave(true);
		
//		for(int a=0; a<20; a++){
//			System.out.println(a +" - "+ cave.RoomToString(a));
//		}
		
		assertTrue(cave.isBatNear());
		assertFalse(cave.isPitNear());
		assertFalse(cave.isWumpusNear());
		
		
	}
}
