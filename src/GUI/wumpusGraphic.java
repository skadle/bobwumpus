package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class wumpusGraphic extends JPanel implements Observer {
	

	private BufferedImage sheet, background;
	private JLabel room1, room2, room3;
	private Point upperLeft;
	private Cursor cursor;
	private JTextField graphicText = new JTextField();
	private wumpusGUI gui;
	
	public wumpusGraphic() {
		loadImages();
		this.setLayout(null);
		room1 = new JLabel("1");
		room1.setBounds(60, 100, 50, 50);
		room1.setForeground(Color.WHITE);
		room2 = new JLabel("2");
		room2.setBounds(230, 100, 50, 50);
		room2.setForeground(Color.WHITE);
		room3 = new JLabel("3");
		room3.setBounds(400, 100, 50, 50);
		room3.setForeground(Color.WHITE);
		
		graphicText.setBounds(50, 450, 400, 20);
		graphicText.setPreferredSize(new Dimension(400, 20));
		graphicText.setEditable(false);
		graphicText.setBackground(Color.black);
		graphicText.setForeground(Color.green);

		this.add(room1);
		this.add(room2);
		this.add(room3);
		this.add(graphicText);
		
		this.addMouseListener(new ClickListener());
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D gr = (Graphics2D) g;

		gr.drawImage(background, 0, 0, null);
	}

	
	private void loadImages() {
		try {
			background = ImageIO.read(new File("images" + File.separator
					+ "cave.jpg"));
		} catch (IOException e) {
			System.out.println("Could not find 'cave.jpg'");
		}
//		try {
//			sheet = ImageIO.read(new File("images" + File.separator
//					+ "DuckHuntOriginalSheet.gif"));
//
//		} catch (IOException e) {
//			System.out.println("Could not find 'DuckHuntOriginalSheet.gif'");
//		}
//		try {
//			Toolkit toolkit = Toolkit.getDefaultToolkit();
//			Image img = ImageIO.read(new File("images" + File.separator
//					+ "CrosshairCursor.png"));
//			cursor = toolkit.createCustomCursor(img, new Point(16, 16),
//					"Crosshair");
//			this.setCursor(cursor);
//		} catch (IOException e) {
//			System.out.println("Could not find 'CrosshairCursor.png'");
//		}
	}
	
	private class ClickListener implements MouseListener {

		@Override
			public void mouseClicked(MouseEvent arg0) {
				if((arg0.getX() > 20 && arg0.getY() > 160) && (arg0.getX() < 120 && arg0.getY() < 280)){
					 System.out.println("shoot arrow to left most turnal");
				}
				else if((arg0.getX() > 200 && arg0.getY() > 180) && (arg0.getX() < 260 && arg0.getY() < 260)){
					System.out.println("shoot arrow to middle turnal");
				}
				else if((arg0.getX() > 360 && arg0.getY() > 190) && (arg0.getX() < 440 && arg0.getY() < 270)){
					System.out.println("shoot arrow to right most turnal");
				}
				else if((arg0.getX() > 20 && arg0.getY() > 300) && (arg0.getX() < 120 && arg0.getY() < 350)){
					 System.out.println("move to left most turnal");
					 //room1.setText("4");
				}
				else if((arg0.getX() > 200 && arg0.getY() > 300) && (arg0.getX() < 260 && arg0.getY() < 350)){
					 System.out.println("move to middle turnal");
				}
				else if((arg0.getX() > 360 && arg0.getY() > 300) && (arg0.getX() < 440 && arg0.getY() < 350)){
					 System.out.println("move to right most turnal");
				}
				else{
					System.out.println(arg0.getX());
				}
			}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			
		}

	}
	
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	

}
