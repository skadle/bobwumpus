package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class wumpusGUI extends JFrame {

	private JButton text = new JButton("text");
	private JButton graphic = new JButton("graphic");
	private JButton switchView = new JButton("Switch View");
	private JButton move = new JButton("Move");
	private JButton shoot = new JButton("Shoot");
	private JButton simple = new JButton("Simple");
	private JButton random = new JButton("Random");
	private JTextField current = new JTextField("current");
	private JTextField where = new JTextField("where");
	private JTextField field = new JTextField("type something");
	private JPanel panel = new JPanel();
	private JPanel option = new JPanel();
	private JPanel textPanel = new JPanel();
	private JPanel graphicPanel = new JPanel();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		wumpusGUI gui = new wumpusGUI();
		gui.setVisible(true);
		//gui.setResizable(false);
	}

	public wumpusGUI() {
		layoutGUI();
		setupListeners();
	}

	private void layoutGUI() {
		this.setLocation(50, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Hunt the Wumpus");

		panel.add(simple);
		panel.add(random);
		this.add(panel);
		this.pack();
	}

	private void setupListeners() {
		ButtonListener buttonListener = new ButtonListener();
		simple.addActionListener(buttonListener);
		random.addActionListener(buttonListener);
		text.addActionListener(buttonListener);
		graphic.addActionListener(buttonListener);
		switchView.addActionListener(buttonListener);
	}

	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			JButton button = (JButton) event.getSource();
			if(button == simple){
				layout2();
			} else if(button == random){
				layout2();
			} else if (button == text) {
				textGUI();
				graphicPanel.setVisible(false);
			} else if (button == graphic) {
				wumpusGraphic();
				textPanel.setVisible(false);
			} else {
				if (textPanel.isVisible()) {
					wumpusGraphic();
					textPanel.setVisible(false);
					graphicPanel.setVisible(true);
				} else {
					textGUI();
					textPanel.setVisible(true);
					graphicPanel.setVisible(false);
				}
			}
			revalidate();
			repaint();
		}
	}
	
	public void textGUI() {
		option.setVisible(false);
		current.setEditable(false);
		current.setBackground(Color.black);
		current.setForeground(Color.green);
		current.setPreferredSize(new Dimension(490, 410));
		where.setPreferredSize(new Dimension(400, 20));
		field.setPreferredSize(new Dimension(400, 20));
		textPanel.add(current);
		textPanel.add(where);
		textPanel.add(move);
		textPanel.add(field);
		textPanel.add(shoot);
		textPanel.add(switchView);
		this.add(textPanel);
		this.setSize(500, 550);
	}

	public void wumpusGraphic() {
		option.setVisible(false);
		graphicPanel = new wumpusGraphic();
		switchView.setBounds(190, 485, 105, 30);
		graphicPanel.add(switchView);
		this.add(graphicPanel);
		this.setSize(500, 550);
		
	}

	public void layout2() {
		panel.setVisible(false);
		option.add(text);
		option.add(graphic);
		this.add(option);
		this.pack();
	}

	

}