package view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

//The moving JPanel is given a vertical speed through walk()/shoot()/fly(),
//then calls moveSprite() for the appropriate speed of movement.
//moveSprite() calls spriteSwitch() which flips between two sprites.

public class GraphicMove {
	private BufferedImage bat1, bat2, walk1, walk2, shoot1, shoot2;

	private void loadSprites() {
		try {
			bat1 = ImageIO
					.read(new File("images" + File.separator + "bat1.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'bat1.bmp'");
		}
		try {
			bat2 = ImageIO
					.read(new File("images" + File.separator + "bat2.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'bat2.bmp'");
		}
		try {
			walk1 = ImageIO.read(new File("images" + File.separator
					+ "walk1.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'walk1.bmp'");
		}
		try {
			walk2 = ImageIO.read(new File("images" + File.separator
					+ "walk2.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'walk2.bmp'");
		}
		try {
			shoot1 = ImageIO.read(new File("images" + File.separator
					+ "shoot1.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'shoot1.bmp'");
		}
		try {
			shoot2 = ImageIO.read(new File("images" + File.separator
					+ "shoot2.bmp"));
		} catch (IOException e) {
			System.out.println("Could not find 'shoot2.bmp'");
		}
	}

	private void moveSprite(JPanel thePanel, int direction, int vertSpeed,
			BufferedImage img1, BufferedImage img2) {
		int x = 120;
		int y = 120;
		int height = 30;
		int width = 20;

		for (int i = 0; i < 100 / vertSpeed; i++) {
			y += vertSpeed;
			x += (direction - 1) * vertSpeed;
			thePanel.setBounds(x, y, height, width);
			if(i%2)
			spriteSwitch(thePanel, i % 2);
		}
	}

	public void walk(JPanel thePanel, int direction) {
		loadSprites();
		int vertSpeed = 2;
		moveSprite(thePanel, direction, vertSpeed);
	}

	public void shoot(JPanel thePanel, int direction) {
		loadSprites();
		int vertSpeed = 5;
		moveSprite(thePanel, direction, vertSpeed);
	}

	public void fly(JPanel thePanel, int direction) {
		loadSprites();
		int vertSpeed = 1;
		moveSprite(thePanel, direction, vertSpeed);
	}

	// shifts back and forth between two moving animations
	private void spriteSwitch(JPanel thePanel, int animationFrame) {

	}
}
