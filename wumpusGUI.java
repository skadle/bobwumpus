package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class wumpusGUI extends JFrame{
	
	private JButton text = new JButton("text");
	private JButton graphic = new JButton("graphic");
	private JButton switchView = new JButton("Switch View");
	private JButton move = new JButton("Move");
	private JButton shoot = new JButton("Shoot");
	private JTextField current = new JTextField("current");
	private JTextField where = new JTextField("where");
	private JTextField field = new JTextField("type something");
	private JPanel panel = new JPanel();
	private JPanel textPanel = new JPanel();
	private JPanel graphicPanel = new JPanel();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame gui = new wumpusGUI();
		gui.setVisible(true);
		gui.setResizable(false);
	}
	
	public wumpusGUI(){
		layoutGUI();
		setupListeners();
	}
	
	private void layoutGUI() {
		this.setLocation(50, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Hunt the Wumpus");
		
		panel.add(text);
		panel.add(graphic);
		this.add(panel);
		this.pack();
	}

	private void setupListeners() {
		ButtonListener buttonListener = new ButtonListener();
		text.addActionListener(buttonListener);
		graphic.addActionListener(buttonListener);
		switchView.addActionListener(buttonListener);
	}

	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			JButton button = (JButton) event.getSource();
			if(button == text){
				textGUI();
				graphicPanel.setVisible(false);
			}
			else if(button == graphic){
				graphicGUI();
				textPanel.setVisible(false);
			}
			else{
				if(textPanel.isVisible()){
					graphicGUI();
					textPanel.setVisible(false);
					graphicPanel.setVisible(true);
				}
				else{
					textGUI();
					graphicPanel.setVisible(false);
					textPanel.setVisible(true);
				}
			}
			revalidate();
			repaint();
		}	
	}

	public void textGUI() {
		panel.setVisible(false);		
		current.setEditable(false);
		current.setBackground(Color.black);
		current.setForeground(Color.green);
		current.setPreferredSize( new Dimension( 498, 410 ) );
		where.setPreferredSize( new Dimension( 400, 20 ) );
		field.setPreferredSize( new Dimension( 400, 20 ) );
		textPanel.add(current);
		textPanel.add(where);
		textPanel.add(move);
		textPanel.add(field);
		textPanel.add(shoot);
		textPanel.add(switchView);
		this.add(textPanel);
		this.setSize(500, 550);
	}

	public void graphicGUI() {
		panel.setVisible(false);
		this.add(switchView);
		this.setSize(500, 550);
	}


}
